const express = require ( "express");
const app = express();
const data = require ("./user.json");
const fs = require ("fs");
const path = require ("path");
const appRouting = require ("'./index.html'");
const router = require ("express").Router();

app.use('/',appRouting);
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use('/assets',express.static(path.join(__dirname, 'assets')));

app.get("/api/posts", (req,res)=>{
    res.json(data);
})

router.get('/', function (req, res) {
    res.render('home');
});

router.get('/contact', function (req, res) {
    res.render('contact');
});

router.get('/about', function (req, res) {
    res.render('about');
});
router.get('/work', function (req, res){
    res.render("work")
})
app.get("/api/posts/:id", (req,res)=>{
    const { id }= req.params;
    const post = data.find ((row)=> row.id == id);
    res.json(post);
})

app.post("/api/posts/", (req,res)=>{
    const {id, email,password,year, gen} = req.body;
    const payload = {id, email, password, year,gen};
    const data1 = [...data, payload]
    fs.writeFileSync("./user.json", JSON.stringify(data1));

    res.json({
        message: "data berhasil ",
        data: payload,

    });
});
app.delete("/api/post/:id", (req,res)=>{
    const { id } = req.params;
    const posts = data.filter((row)=> row.id != id);
    fs.writeFileSync("./user.json", JSON.stringify(posts));

    res.json({
        message: `hapus data dengan id berhasil :${id}`,
    });

});

app.put("/api/post/:id",(req,res) => {
    const {id} =req.params;
    const {password,gen} = req.body;
    
    let post = data.find((row)=> row.id == id);

    const posts = data.map ((row)=>(row.id == id? post : row));

    fs.writeFileSync("./user.json", JSON.stringify(post));


    res.json({
        message: `update berhasil`,
        data: post,
    });
});

app.use(express.json());
app.use(express.urlencoded({extended: false}));

const validation = (req,res, next)=>{
    const {id} = req. body;
    const isFind = data.find((row) =>row.id == id);
    if (isFind) {
        res.status(400).json({message: "id tidak ada"});

    } else (
        next ()
    )
};
app.set('views',path.join(__dirname,'views'));
app.set('view engine', 'ejs');










app.listen(8000,() => console.log(`Listening at http://localhost:${8000}`))

module.exports=app;