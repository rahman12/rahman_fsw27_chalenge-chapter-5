function getPilihanComputer (){
    const comp = math.random();
  
    if( comp < 0.34 ) return 'batu';
    if( comp >= 0.34 && comp < 0.67 ) return 'gunting';
      return'kertas';
  }
  
  function gethasil(comp, player){
    
      if( player == comp ) return 'SERI!';
      if( player == 'batu' ) return ( comp == 'gunting' ) ? 'Player1 WIN' : 'COM WIN';
      if( player == 'gunting' ) return ( comp == 'batu' ) ? 'COM WIN' : 'Player1 WIN';
      if( player == 'kertas' ) return ( comp == 'orang' ) ? 'COM WIN' : 'Player1 WIN';
      }
  
  const pbatu = document.querySelector ('.batu');
  pbatu.addEventListener ('click', function(){
    const PilihanComputer = getPilihanComputer();
    const pilihanPlayer = pbatu.className;
    const hasil = gethasil (PilihanComputer, pilihanPlayer);
    const pilihBatu = document.querySelector ('.batu');
    pilihBatu.setAttribute ('src','assets/' + pilihanComputer + '.png');
    
    const info = document.querySelector('.info');
    info.innerHTML = hasil;
  });
  
  
  
  const pgunting = document.querySelector ('.gunting');
  pgunting.addEventListener ('click', function(){
    const PilihanComputer = getPilihanComputer();
    const pilihanPlayer = pgunting.className;
    const hasil = gethasil (PilihanComputer, pilihanPlayer);
    const pilihGunting = document.querySelector ('.gunting');
    pilihGunting.setAttribute ('src','assets/' + pilihanComputer + '.png');
    
    const info = document.querySelector('.info');
    info.innerHTML = hasil;
  });
  
  const pkertas = document.querySelector ('.kertas');
  pkertas.addEventListener ('click', function(){
    const PilihanComputer = getPilihanComputer();
    const pilihanPlayer = pkertas.className;
    const hasil = gethasil (PilihanComputer, pilihanPlayer);
    const pilihKertas = document.querySelector ('.kertas');
    pilihKertas.setAttribute ('src','assets/' + pilihanComputer + '.png');
    
    const info = document.querySelector('.info');
    info.innerHTML = hasil;
  });
  
  const reset = document.querySelector(".refresh");
  reset.addEventListener ("click", function(){
    window.location.reload();
  
  })